provider "aws" {
    region = "ap-south-1"
}

terraform {
  backend "s3" {
    bucket          = "batch47-remotestate-dynamodbazhar"
    key             = "dev1/ec2/terraform.tfstate"
    region          = "ap-south-1"
    dynamodb_table  = "terraform-locking"
  }
}
