resource "aws_instance" "sample" {
  ami                     = "ami-050116d1c1a681b16"
  instance_type           = "t2.micro"
  vpc_security_group_ids  = [aws_security_group.allow_ssh.id]
  key_name                = "tffffff"

  tags = {
    Name = "Test-Machine-Remote-State"
  }

# Copies the private key file as the centos user using SSH
provisioner "file" {
  source      = "/home/centos/.ssh/id_rsa"
  destination = "/home/centos/.ssh/id_rsa"

  connection {
    type        = "ssh"
        user        = "centos"
    private_key = "${file("/home/centos/.ssh/id_rsa")}"
    host        = aws_instance.sample.private_ip
     }
   }

provisioner "remote-exec" {
  connection {
    type        = "ssh"
    user        = "centos"
    private_key = "${file("/home/centos/.ssh/id_rsa")}"
    host        = aws_instance.sample.private_ip
     }

    inline = [
      "echo -e 'Host * \n \t StrictHostKeyChecking no' > /home/centos/.ssh/config",
      "chmod 600 /home/centos/.ssh/id_rsa  /home/centos/.ssh/config",
      "sudo yum install ansible git -y",
      "ansible-pull -U git@gitlab.com:clouddevops-b47/ansible.git stack-pull.yml"
    ]
  }
}


# Preserving the Private Ip in a file which can be consumed by ANSIBLE 
resource "local_file" "private_ip" {
    content  = "${aws_instance.sample.private_ip}"
    filename = "/tmp/tf-hosts"
}
