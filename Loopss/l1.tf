provider "aws" {
    region = "ap-south-1"
}


resource "aws_instance" "sample" {
  ami                     = "ami-0667149a69bc2c367"
  count=5
  instance_type           = "t2.micro"

  tags = {
    Name = "Loopmachine${count.index}"
  }
}
