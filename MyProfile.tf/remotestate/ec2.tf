resource "aws_instance" "sample" {
  ami           = "ami-0667149a69bc2c367"
  instance_type = "t2.micro"
   vpc_security_group_ids = [aws_security_group.allow_ssh.id] 
   key_name = "tffffff"

  tags = {
    Name = "final-remote1"
  }

  provisioner "file" {
  source      = "/home/centos/.ssh/id_rsa"
  destination = "/home/centos/.ssh/id_rsa"

  connection {
    type        = "ssh"
    user        = "centos"
    private_key = "${file("/home/centos/.ssh/id_rsa")}"
    host        = aws_instance.sample.private_ip
     }
   }
}



resource "aws_security_group" "allow_ssh" {
  name        = "allow_ssh-tf"
  description = "Allow ssh inbound traffic"

  ingress {
    description      = "ssh from VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]

  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_ssh"
  }
}
