
provider "aws" {

    region= "ap-south-1"
   
}

terraform {
  backend "s3" {
    bucket = "bacth47terraformazhar"
    key    = "dev/ec2/terraform.tfstate"
    region = "ap-south-1"
  }
}