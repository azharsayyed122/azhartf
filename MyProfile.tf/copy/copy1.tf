resource "aws_instance" "sample" {
  ami                     = "ami-050116d1c1a681b16"
  instance_type           = "t2.micro"
  key_name                = "lab"

  tags = {
    Name = "Test-Machine-Remote-State"
  }

# Copies the private key file as the centos user using SSH
provisioner "file" {
  source      = "/home/centos/.ssh/id_rsa"
  destination = "/home/centos/.ssh/id_rsa"

  connection {
    type        = "ssh"
    user        = "centos"
    private_key = "${file("/home/centos/.ssh/id_rsa")}"
    host        = aws_instance.sample.private_ip
     }
   }
}