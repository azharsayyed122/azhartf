provider "aws" {
    region = "ap-south-1"
}


resource "aws_instance" "sample" {
  ami                     = "ami-050116d1c1a681b16"
  count=2
  instance_type           = "t2.micro"
  vpc_security_group_ids  = [aws_security_group.allow_ssh.id]
  key_name                = "tffffff"

  tags = {
    Name = "Loopmachine"
  }
}
